<?php


namespace Soen\Pool;


interface PoolInterface
{
    /**
     * 创建连接
     * @return mixed
     */
    public function createConnection():Connection;

    /**
     * 复用连接
     * @return mixed
     */
    public function reuse();

    /**
     * 归还连接
     * @return mixed
     */
    public function revert(Connection $connection);

    /**
     * 连接入池
     * @return mixed
     */
    public function push(Connection $connection);

    /**
     * 连接弹出
     * @return mixed
     */
    public function pop();
}