<?php


namespace Soen\Pool;


class Driver
{
    /**
     * @var Pool
     */
    protected $pool;
    /**
     * @var Connection
     */
    protected $connection;

    public function close(){
        $this->pool->revert($this->connection, true);
    }

    /**
     * @param Pool $pool
     */
    public function setPool (Pool $pool) {
        $this->pool = $pool;
    }

    /**
     * @return Pool
     */
    public function getPool(){
        return $this->pool;
    }

    /**
     * @param Connection $connection
     */
    public function setConn (Connection $connection) {
        $this->connection = $connection;
    }

    /**
     * @return Connection
     */
    public function getConn () {
        return $this->connection;
    }
}