<?php


namespace Soen\Pool;


interface DriverInterface
{
    public function create();
    public function close();
}