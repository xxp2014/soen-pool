<?php


namespace Soen\Pool;


class Connection
{
    public $driver;
    /**
     * @var Driver
     */
    public $connection;
    protected $hashId;
    public function __construct($driver)
    {
        $this->driver = $driver;
        $this->create();
        $this->hashId = spl_object_hash($this);
    }

    /**
     * @return \Redis
     */
    public function create(){
//        if($this->connection){
//            return $this->connection;
//        }
        return $this->connection = $this->driver->create();
    }

    /**
     * @return Driver
     */
    public function getConnection () {
        return $this->connection;
    }
    
    public function getHashId(){
        return $this->hashId;
    }


}